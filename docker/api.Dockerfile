from node:lts-alpine

WORKDIR /home/node/app
COPY *.json /home/node/app/
COPY src/server/package*.json /home/node/app/src/server/
COPY packages/logger/package*.json /home/node/app/packages/logger/
COPY packages/koa-logger/package*.json /home/node/app/packages/koa-logger/

RUN npm ci --unsafe-perm

COPY . /home/node/app

cmd [ "npm", "start" ]
