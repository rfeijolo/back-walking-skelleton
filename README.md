# How to run

* Install [Docker](https://www.docker.com/get-started)
* Run `docker-compose up`
* Check with `curl http://localhost:3000/health` the response should have a status 204 (No-Content)
