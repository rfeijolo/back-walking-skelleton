const Koa = require ('koa');
const { createContainer } = require('awilix');
const { loadControllers, scopePerRequest } = require('awilix-koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const respond = require('koa-respond');

const httpLogger = require('koa-logger');
const logger = require('logger');
const database = require('database');

const server = new Koa();
const container = createContainer();

server.use(httpLogger());
server.use(cors());
server.use(bodyParser());
server.use(respond());
server.use(scopePerRequest(container));
server.use(loadControllers('routes/*.js', { cwd: __dirname })); //eslint-disable-line no-undef

const port = process.env.PORT || 3000;
server.listen(port);

logger.info(`API listening on port ${port}`);

module.exports = server;
