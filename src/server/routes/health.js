const { createController } = require('awilix-koa');
const NO_CONTENT = 204;

const healthController = () => ({
  getHealth
});

module.exports = createController(healthController)
  .prefix('/health')
  .get('/', 'getHealth');

async function getHealth(ctx) {
  ctx.status = NO_CONTENT;
}

