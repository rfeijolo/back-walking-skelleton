const { createController } = require('awilix-koa');
const { Task } = require('database').models;

const todoController = () => ({
  getTodos,
  postTodos,
  deleteTodos,
  putTodos,
});

module.exports = createController(todoController)
  .prefix('/todos')
  .get('/', 'getTodos')
  .post('/', 'postTodos')
  .put('/:uuid', 'putTodos')
  .delete('/:uuid', 'deleteTodos');

async function getTodos(ctx) {
  const tasks = await Task.findAll().map(({ dataValues }) => dataValues);
  ctx.ok(tasks);
}

async function postTodos(ctx) {
  const { body: task } = ctx.request;
  const createdTask = await Task.create(task);
  ctx.set('Location', `/todos/${createdTask.uuid}`);
  ctx.created(createdTask);
}

async function putTodos(ctx) {
  const { body: task } = ctx.request;
  const { uuid } = ctx.params;
  const createdTask = await Task.update(task,{where: { uuid: uuid }});
  ctx.set('Location', `/todos/${createdTask.uuid}`);
  ctx.created(createdTask);
}

async function deleteTodos(ctx) {
  const { uuid } = ctx.params;
  await Task.destroy({ where: { uuid }});
  ctx.noContent();
}
