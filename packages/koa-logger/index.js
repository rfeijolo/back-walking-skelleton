const logger = require('logger');

module.exports = createHttpLogger;

function createHttpLogger() {
  return async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
    logger.info(`${ctx.method} ${ctx.url} - ${ms}ms`);
  };
}
