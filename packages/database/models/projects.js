const { STRING, UUID, UUIDV4 } = require('sequelize');

const createProject = ({ sequelize }) => (
  sequelize.defined('project', {
    uuid: {
      type: UUID,
      defaultValue: UUIDV4,
      primaryKey: true,
    },
    description: {
      type: STRING,
      allowNull: false,
    }
  })
);

module.exports = createProject;