const { STRING, DATE, UUID, UUIDV4, ENUM } = require('sequelize');

const createTask = ({ sequelize }) => (
  sequelize.define('task', {
    uuid: {
      type: UUID,
      defaultValue: UUIDV4,
      primaryKey: true,
    },
    description: {
      type: STRING,
      allowNull: false,
    },
    due: {
      type: DATE,
      allowNull: false,
    },
    priority: {
      type: ENUM('NORMAL', 'MEDIUM', 'HIGH'),
      allowNull: false,
    },
    status: {
      type: ENUM('PENDING', 'COMPLETED', 'DELETED', 'WAITING'),
      allowNull: false,
    },
    end: {
      type: DATE,
    },
    start: {
      type: DATE,
    },
  })
);

module.exports = createTask;
