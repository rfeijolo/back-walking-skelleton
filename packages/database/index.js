const databaseUrl = process.env.DATABASE_URL;
const createTask = require('./models/task');
const createProject = require('./models/project');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(databaseUrl);

const createModels = ({ sequelize }) => ({
  Task: createTask({ sequelize }),
  Project: createProject({ sequelize }),
});


(async () => {
  await sequelize.sync({ force: true })
})();

module.exports = {
  models: createModels({ sequelize }),
};
