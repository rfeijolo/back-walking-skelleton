const winston = require('winston');
const { format } = require('logform');

const isProduction = process.env.NODE_ENV === 'production';

const alignedWithColorsAndTime = format.combine(
  format.colorize(),
  format.timestamp(),
  format.align(),
  format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);

const logger = winston.createLogger({
  level: isProduction ? 'info' : 'silly',
  format: alignedWithColorsAndTime,
});

if(!isProduction) {
  logger.add(new winston.transports.Console());
}

module.exports = logger;
